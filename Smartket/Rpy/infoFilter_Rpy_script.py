#!/usr/bin/python3
# -*- coding: utf-8 -*-

from urllib import request

# run with python3
# Retrieve the webpage as a string
response = request.urlopen("http://api.datosabiertos.msi.gob.pe/api/v2/datastreams/LICEN-DE-FUNCI/data.csv/?auth_key=642781e995c0dadf9a9483df4fb9f95f978c89ab&limit=10000&")
csv = response.read()

# Save the string to a file
csvstr = str(csv).strip("b'")

lines = csvstr.split("\\n")
f = open("LICENCIAS_DE_FUNCIONAMIENTO.csv", "w")
for line in lines:
   f.write(line + "\n")
f.close()
